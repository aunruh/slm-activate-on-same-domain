<?php
/*
Plugin Name: Slm Activate on same Domain
Version: 0.0.1
Plugin URI: 
Author: 
Author URI: 
Description: When activated, "Software License Manager" Plugin will let you activate multiple times on same domain.
*/

class SlmAosd{

	public function __construct(){
		add_action('slm_api_listener_slm_activate', array($this, 'slm_activate'));
	}

	// i just copied all the code that came after "do_action('slm_api_listener_slm_activate');"
	public static function slm_activate(){
		$fields = array();
		$fields['lic_key'] = trim(strip_tags($_REQUEST['license_key']));
		$fields['registered_domain'] = trim(strip_tags($_REQUEST['registered_domain'])); //gethostbyaddr($_SERVER['REMOTE_ADDR']);
		$fields['item_reference'] = trim(strip_tags($_REQUEST['item_reference']));
		// $slm_debug_logger->log_debug("License key: " . $fields['lic_key'] . " Domain: " . $fields['registered_domain']);

		global $wpdb;
		$tbl_name = SLM_TBL_LICENSE_KEYS;
		$reg_table = SLM_TBL_LIC_DOMAIN;
		$key = $fields['lic_key'];
		$sql_prep1 = $wpdb->prepare("SELECT * FROM $tbl_name WHERE license_key = %s", $key);
		$retLic = $wpdb->get_row($sql_prep1, OBJECT);

		$sql_prep2 = $wpdb->prepare("SELECT * FROM $reg_table WHERE lic_key = %s", $key);
		$reg_domains = $wpdb->get_results($sql_prep2, OBJECT);
		if ($retLic) {
		    if ($retLic->lic_status == 'blocked') {
		        $args = (array('result' => 'error', 'message' => 'Your License key is blocked'));
		        SLM_API_Utility::output_api_response($args);
		    } elseif ($retLic->lic_status == 'expired') {
		        $args = (array('result' => 'error', 'message' => 'Your License key has expired'));
		        SLM_API_Utility::output_api_response($args);
		    }
		    
		    // *I CHANGED STH HERE:*
		    // check if this domain was already activated
		    foreach ($reg_domains as $reg_domain) {
		    	if ($fields['registered_domain'] == $reg_domain->registered_domain) {
		    		$args = (array('result' => 'success', 'message' => 'License key activated'));
		    		SLM_API_Utility::output_api_response($args);
	    		}		    
	    	}

		    if (count($reg_domains) < floor($retLic->max_allowed_domains)) {
		        foreach ($reg_domains as $reg_domain) {
		            if (isset($_REQUEST['migrate_from']) && (trim($_REQUEST['migrate_from']) == $reg_domain->registered_domain)) {
		                $wpdb->update($reg_table, array('registered_domain' => $fields['registered_domain']), array('registered_domain' => trim(strip_tags($_REQUEST['migrate_from']))));
		                $args = (array('result' => 'success', 'message' => 'Registered domain has been updated'));
		                SLM_API_Utility::output_api_response($args);
		            }
		            if ($fields['registered_domain'] == $reg_domain->registered_domain) {
		            	// *I CHANGED STH HERE:*

		            	// $args = (array('result' => 'error', 'message' => 'License key already in use on ' . $reg_domain->registered_domain));
		                $args = (array('result' => 'success', 'message' => 'License key activated'));
		                SLM_API_Utility::output_api_response($args);
		            }
		        }
		        $fields['lic_key_id'] = $retLic->id;
		        $wpdb->insert($reg_table, $fields);
		        
		        // $slm_debug_logger->log_debug("Updating license key status to active.");
		        $data = array('lic_status' => 'active');
		        $where = array('id' => $retLic->id);
		        $updated = $wpdb->update($tbl_name, $data, $where);
		        
		        $args = (array('result' => 'success', 'message' => 'License key activated'));
		        SLM_API_Utility::output_api_response($args);
		    } else {
		        $args = (array('result' => 'error', 'message' => 'Reached maximum allowable domains'));
		        SLM_API_Utility::output_api_response($args);
		    }
		} else {
		    $args = (array('result' => 'error', 'message' => 'Invalid license key'));
		    SLM_API_Utility::output_api_response($args);
		}
	}

}

new SlmAosd();